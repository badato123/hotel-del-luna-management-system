/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package resort.del.luna;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.ResultSet;

/**
 *
 * @author Bil Badato
 */
public class DBHelper {
    Connection con = null;
    Statement stmt = null;
    
public void connectDB() throws Exception{
    
    con = DriverManager.getConnection("jdbc:derby://localhost:1527/dbClientDetailReservation", "Bil", "badato123");
    System.out.println("Connected to the database");
    
   } 
public boolean insertRecord(int CustomerID , String Username, String Password ){
        boolean flag = false;
        try {
            stmt = con.createStatement();
            String sql = "Insert into tblRegistration Values ("+CustomerID+", '"+Username+"', '"+Password+"')";

            
            if (stmt.executeUpdate(sql) == 1)
                flag = true;
      } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
         
            return flag;
    }   
public boolean insertRecord2(String Name , String MobileNumber, String DateReservation, double price, int CustomerID){
      
      boolean flag = false;
        try {
            stmt = con.createStatement();
            String sql = "Insert into tblClientDetailsReservation Values ('"+Name+"', '"+MobileNumber+"', '"+DateReservation+"',"+price+","+CustomerID+")";
            
            if (stmt.executeUpdate(sql) == 1)
                flag = true;
      } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
         
            return flag;
    }   
public ResultSet displayAllRecords(){
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            String sql = "Select * from  tblRegistration";
             rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    
    }
public ResultSet displayAllRecords1(){
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            String sql = "Select * from  tblClientDetailsReservation";
             rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    
    }
public ResultSet displayByUsername(String Username){
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            String sql = "Select * from  tblRegistration where Username = '"+Username+"";
             rs = stmt.executeQuery(sql); 
        } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    
    }
public ResultSet DisplayCustomerID(int CustomerID){
        ResultSet  rs = null;
        try {
            stmt = con.createStatement();
            String sql = "select * from tblClientDetailsReservation where CustomerID = "+CustomerID+"";
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

 public ResultSet DisplayName(String Name){
        ResultSet  rs = null;
        try {
            stmt = con.createStatement();
            String sql = "select * from tblClientDetailsReservation where Name = '"+Name+"'";
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
  public ResultSet DisplayMobileNumber(String MobileNumber){
        ResultSet  rs = null;
        try {
            stmt = con.createStatement();
            String sql = "select * from tblClientDetailsReservation where MobileNumber = '"+MobileNumber+"'";
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
 public ResultSet DisplayDateReservation(String DateReservation){
        ResultSet  rs = null;
        try {
            stmt = con.createStatement();
            String sql = "select * from tblClientDetailsReservation where DateReservation = '"+DateReservation+"'";
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
        
    }
 
public boolean updateRecord(int CustomerID , String Name, String MobileNumber, String DateReservation ){
        boolean flag = false;
        try {
            stmt = con.createStatement();

        String sql = "Update tblClientDetailsReservation set Name= '"+Name+"',MobileNumber = '"+MobileNumber+"', DateReservation = '"+DateReservation+"' Where CustomerID = "+CustomerID+"";
           
            if (stmt.executeUpdate(sql) == 1)
                flag = true;
      } catch (SQLException ex) {
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
         
            return flag;
    }
public boolean deleteRecord(int CustomerID){
        boolean flag = false;
        try { 
            stmt = con.createStatement();
            String sql = "Delete from tblClientDetailsReservation where CustomerID = "+CustomerID+"";
               
            if(stmt.executeUpdate(sql) == 1);
                flag = true;
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return flag;
    }
public boolean deleteRecord1(int CustomerID){
        boolean flag = false;
        try { 
            stmt = con.createStatement();
            String sql = "Delete from tblRegistration where CustomerID = "+CustomerID+"";
               
            if(stmt.executeUpdate(sql) == 1);
                flag = true;
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return flag;
    }
 public ResultSet DisplayCustomerID1(int CustomerID){
        ResultSet  rs = null;
        try {
            stmt = con.createStatement();
            String sql = "select * from tblRegistration where CustomerID = "+CustomerID+"";
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex){
            Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
 
}



